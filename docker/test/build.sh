#!/bin/bash

set -exu

new_groonga_version=$1
old_groonga_version=$2

function build_groonga () {
  if [ "master" = $1 ]; then
    git clone https://github.com/groonga/groonga.git groonga-$1
  else
    wget https://packages.groonga.org/source/groonga/groonga-$1.tar.gz
    tar -zxvf groonga-$1.tar.gz
  fi
  pushd groonga-$1
  ./configure --prefix=/tmp/local/$2
  make; make install
  popd
}

mkdir -p /build/new /build/old

pushd /build/new
build_groonga ${new_groonga_version} new
popd

pushd /build/old
build_groonga ${old_groonga_version} old
popd
