#!/bin/bash

set -e

: ${OLD_GROONGA_PREFIX:=/tmp/local/groonga_old}
: ${NEW_GROONGA_PREFIX:=/tmp/local/groonga_new}

run_regression_test=groonga-query-log-run-regression-test
run_regression_test_args=()
run_regression_test_args+=(--old-groonga="${OLD_GROONGA_PREFIX}/bin/groonga")
run_regression_test_args+=(--new-groonga="${NEW_GROONGA_PREFIX}/bin/groonga")
run_regression_test_args+=(--target-command-names=select)
run_regression_test_args+=(--stop-on-failure)
run_regression_test_args+=(--read-timeout=-1)
run_regression_test_args+=(--n-clients=$(nproc))
run_regression_test_args+=(--smtp-server=***)
run_regression_test_args+=(--smtp-auth-user=***)
run_regression_test_args+=(--smtp-auth-password=***)
run_regression_test_args+=(--smtp-starttls)
run_regression_test_args+=(--smtp-port=***)
run_regression_test_args+=(--mail-from=***)
run_regression_test_args+=(--mail-to=***)
run_regression_test_args+=(--mail-subject-on-failure="[Regression] Failure")
run_regression_test_args+=(--mail-only-on-failure)
${run_regression_test} \
  "${run_regression_test_args[@]}" \
  "$@"
